import { getCardProvider } from "../src/getCardProvider";

// given
// when
// then

test('Should recognize MasterCard', () => {
    // given
    const num: number = 5555555555554444;
    // when
    const result = getCardProvider(num);
    // then
    expect(result).toBe('Master Card');
})

test('Should recognize Visa', () => {
    // given
    const num: number = 4012888888881881;
    // when
    const result = getCardProvider(num);
    // then
    expect(result).toBe('Visa');
})

test('Should recognize American Express', () => {
    // given
    const num: number = 378282246310005;
    // when
    const result = getCardProvider(num);
    // then
    expect(result).toBe('American Express');
})

test('Should return num as incorrect', () => {
    // given
    const num: number = 123;
    // when
    const result = getCardProvider(num);
    // then
    expect(result).toBe('Incorrect number');
})

test('Should throw if provider is invalid', () => {
    // given
    const num: number = 3566002020360505;
    // then
    expect(() => {
        getCardProvider(num);
    }).toThrow('Cannot recognize card provider');
})