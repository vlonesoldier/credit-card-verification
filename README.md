# Credit Card Validation (typescript, jest, tdd)

Each credit card has its own unique number. Banks have database full of card numbers but inquiring them is extremely slow. Meanwhile, internet swarms with lots of typos and imposters, thus banks need an algorithm that deal with card verification.

Issued credit card number has specified length for each provider:

- Master Card: 16 ciphers.
- Visa: 13 or 16 ciphers.
- American Express: 15 ciphers.

These numbers start with identified set of figures:

- Master Card: 51, 52, 53, 54, 55
- Visa: 4
- American Express: 34, 37

Moreover, the card number, regardless of the provider, must be verified by Luhn's algorithm.