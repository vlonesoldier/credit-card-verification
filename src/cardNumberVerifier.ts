import { normalize } from "./cardNormalizer";

export function isCardNumberValid(cardNumber: number): boolean {

    const normalizedCardNumber = normalize(cardNumber);

    const firstSet: Array<number> = normalizedCardNumber
        .filter((number: number, index: number) => index % 2 === 0);

    const secondSet: Array<number> = normalizedCardNumber
        .filter((number: number, index: number) => index % 2 === 1);

    const firstSum: number = firstSet
        .map((n) => (n * 2).toString())
        .reduce((current, next: string) => {
            return current + Number(next[0]) + (next[1] ? Number(next[1]) : 0)
        }, 0);

    const secondSum: number = secondSet.reduce((current: number, next: number) => current + next, 0);

    return (firstSum + secondSum) % 10 === 0;
}
