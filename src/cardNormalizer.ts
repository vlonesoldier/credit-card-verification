export function normalize(cardNumber: number): Array<number> {
    return cardNumber.toString().length % 2 === 0
        ? [...cardNumber.toString()].map(n => Number(n))
        : [0, ...cardNumber.toString()].map(n => Number(n))

}