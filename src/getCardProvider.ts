import { isCardNumberValid } from "./cardNumberVerifier";

export function getCardProvider(cardNumber: number): string {

    if (isCardNumberValid(cardNumber)) {
        return getProvider(cardNumber);
    } else {
        return 'Incorrect number';
    }
}


function getProvider(cardNumber: number) {
    const cardNumberString = cardNumber.toString();

    if (isVisa(cardNumberString)) {
        return 'Visa';
    }
    else if (isMasterCard(cardNumberString)) {
        return 'Master Card';
    }
    else if (isAmEx(cardNumberString)) {
        return 'American Express';
    }
    else {
        throw new Error('Cannot recognize card provider');
    }
}

function isVisa(cardNumberString: string): boolean {
    const isCorrectLength = cardNumberString.length === 13 || cardNumberString.length === 16;
    const isPrefixIncluded = cardNumberString.startsWith('4');

    return isCorrectLength && isPrefixIncluded;
}

function isMasterCard(cardNumberString: string): boolean {
    const prefixList = ['51', '52', '53', '54', '55'];
    const isCorrectLength = cardNumberString.length === 16;
    const isPrefixIncluded = prefixList.includes(cardNumberString.substring(0, 2));

    return isCorrectLength && isPrefixIncluded;
}

function isAmEx(cardNumberString: string): boolean {
    const prefixList = ['34', '37'];
    const isCorrectLength = cardNumberString.length === 15;
    const isPrefixIncluded = prefixList.includes(cardNumberString.substring(0, 2));

    return isCorrectLength && isPrefixIncluded;
}